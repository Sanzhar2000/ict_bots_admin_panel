<?php

namespace Database\Seeders;

use App\Models\User;
use Faker\Generator as Faker;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class UserSeeder extends Seeder
{
    public $faker;

    public function __construct(Faker $faker)
    {
        $this->faker = $faker;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Admin
        $sanzhar = User::create([
            'name' => 'Sanzhar Toktassyn',
            'email' => 'stoktasyn@gmail.com',
            'email_verified_at' => now(),
            'password' => bcrypt('123456789')
        ]);
        $sanzhar->assignRole(Role::where('name', 'admin')->first());

        // Admin
        $niyazbek = User::create([
            'name' => 'Niyazbek Amantay',
            'email' => 'niyazbek@st2101.aitu',
            'email_verified_at' => now(),
            'password' => bcrypt('123456789')
        ]);
        $niyazbek->assignRole(Role::where('name', 'admin')->first());

        // Admin
        $danila = User::create([
            'name' => 'Danila Malyshko',
            'email' => 'danila@st2101.aitu',
            'email_verified_at' => now(),
            'password' => bcrypt('qwerty123')
        ]);
        $danila->assignRole(Role::where('name', 'admin')->first());

        // Admin
        $alimzhan = User::create([
            'name' => 'Alimzhan Ospan',
            'email' => 'alimzhan@st2101.aitu',
            'email_verified_at' => now(),
            'password' => bcrypt('987654321')
        ]);
        $danila->assignRole(Role::where('name', 'admin')->first());

        // Fake users
        foreach (range(1, 10) as $i) {
            User::create([
                'name' => $this->faker->name,
                'password' => bcrypt('password'),
                'email' => $this->faker->email,
            ]);
        }
    }
}
