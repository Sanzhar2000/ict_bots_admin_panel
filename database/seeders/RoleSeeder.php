<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            'admin',
            'manager',
            'unauthorized_user'
        ];

        foreach($roles as $role) {
            Role::create(['name' => $role]);
        }

        $role_admin = Role::where('name', 'admin')->first();
        $role_admin->syncPermissions(Permission::pluck('id', 'id')->all());

        $manager_permissions = [
            'role-list',
            'role-create',
            'role-edit',
            'permission-list',
            'permission-create',
            'permission-edit',
            'user-list',
            'user-create',
            'user-edit',
            'record-list',
            'record-create',
            'record-edit',
        ];

        $role_manager = Role::where('name', 'manager')->first();
        $role_manager->syncPermissions($manager_permissions);
    }
}
