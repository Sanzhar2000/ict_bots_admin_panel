<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            PermissionSeeder::class,
            RoleSeeder::class,
            UserSeeder::class,
            CourseSeeder::class,
            InstructorSeeder::class,
            TimeSeeder::class,
            DaySeeder::class,
            TypeSeeder::class,
            GroupSeeder::class,
            TimetableSeeder::class
        ]);
        // \App\Models\User::factory(10)->create();
    }
}
