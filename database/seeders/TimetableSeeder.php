<?php

namespace Database\Seeders;

use App\Models\Timetable;
use Illuminate\Database\Seeder;

class TimetableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $timetables  = [
            [1, 1, 1, 'online', 1, 1],
            [1, 2, 1, 'online', 1, 1],

            [2, 3, 3, 'online', 2, 3],
            [2, 4, 5, 'online', 2, 4],
            [2, 8, 6, 'online', 2, 5],
            [2, 8, 6, 'online', 2, 5],

            [3, 1, 6, 'online', 1, 6],
            [3, 2, 6, 'online', 1, 6],
            [3, 3, 1, 'online', 1, 1],
            [3, 9, 7, 'C1-355P', 2, 8],
            [3, 9, 8, 'C1-350P', 2, 9],

            [4, 1, 6, 'online', 1, 6],
            [4, 3, 3, 'online', 1, 3],
            [4, 3, 3, 'online', 1, 3],
            [4, 5, 9, 'C1-235P', 2, 8],
            [4, 5, 10, 'C1-344P', 2, 9],
            [4, 6, 11, 'C1-235P', 2, 8],
            [4, 6, 12, 'C1-344P', 2, 9],

            [5, 6, 3, 'C1-361K', 2, 3],
            [5, 7, 3, 'C1-361K', 2, 3],

            [6, 3, 1, 'C1-361K', 2, 7],
            [6, 4, 1, 'C1-361K', 2, 7],
            [6, 6, 4, 'C1-245K', 2, 10],
            [6, 7, 5, 'gym', 2, 4]
        ];

        foreach ($timetables as $timetable) {
            Timetable::create([
                'group_id' => 1,
                'day_id' => $timetable[0],
                'time_id' => $timetable[1],
                'course_id' => $timetable[2],
                'classroom' => $timetable[3],
                'type_id' => $timetable[4],
                'instructor_id' => $timetable[5]
            ]);
        }
    }
}
