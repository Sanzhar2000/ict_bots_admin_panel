<?php

namespace Database\Seeders;

use App\Models\Instructor;
use Illuminate\Database\Seeder;

class InstructorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $names = [
            'Kassabek Samat',
            'Baizhanova Ainur',
            'Yespenbetova Dana',
            'Shayakhmetov Nurlybek',
            'Mamytova Saule',
            'Auzhanova Assel',
            'Kassabek Dina',
            'Tuselbayeva Zhanar',
            'Burbekova Saule',
            'Smagulova Gulnur'
        ];

        foreach ($names as $name) {
            Instructor::create([
                'name' => $name
            ]);
        }
    }
}
