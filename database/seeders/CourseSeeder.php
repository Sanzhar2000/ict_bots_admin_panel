<?php

namespace Database\Seeders;

use App\Models\Course;
use Illuminate\Database\Seeder;

class CourseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $titles = [
            'Calculus 1',
            'German Language',
            'Introduction to Programming (C++)',
            'Information and Communication Technologies',
            'Physical Culture',
            'Modern History of Kazakhstan',
            'English82',
            'English132',
            'English85',
            'English130',
            'English86',
            'English131',
            'Cultural Studies'
        ];

        foreach ($titles as $title) {
            Course::create([
                'title' => $title
            ]);
        }
    }
}
