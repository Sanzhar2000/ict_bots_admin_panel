<?php

namespace Database\Seeders;

use App\Models\Group;
use Illuminate\Database\Seeder;

class GroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $groups = [
            'ST-2101',
            'ST-2102',
            'TS-2101',
            'TS-2102'
        ];

        foreach ($groups as $group) {
            Group::create([
                'title' => $group
            ]);
        }
    }
}
