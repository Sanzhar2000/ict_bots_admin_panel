<?php

namespace Database\Seeders;

use App\Models\Time;
use Illuminate\Database\Seeder;

class TimeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $times = [
            '08:30-09:20',
            '09:30-10:20',
            '10:40-11:30',
            '11:40-12:30',
            '12:40-13:30',
            '14:00-14:50',
            '15:00-15:50',
            '16:10-17:00',
            '17:10-18:00',
        ];

        foreach ($times as $time) {
            Time::create([
                'title' => $time
            ]);
        }
    }
}
