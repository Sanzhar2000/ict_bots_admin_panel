<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;


use App\Http\Controllers\Admin\ProfileController;
use App\Http\Controllers\Admin\RoleController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\GroupController;
use App\Http\Controllers\Admin\TimetableController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/test', function () {
    $group = \App\Models\Group::find(1);


    return $group->timetables;
//    return [$timetable->group, $timetable->day, $timetable->time, $timetable->course, $timetable->classroom, $timetable->instructor];
});

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::redirect('/panel', '/panel/profile/show')->name('panel.main.url');

///** Admin Panel */
//Route::get('panel/login', [LoginController::class, 'showPanelLoginForm'])->name('panel.login');

//Route::prefix('panel')->middleware(['auth', 'role:admin|manager'])->name('panel.')->group(function ($router) {
Route::prefix('panel')->middleware(['auth'])->name('panel.')->group(function ($router) {
    /** Profile */
    $router->prefix('profile')->name('profile.')->group(function ($router) {
        $router->get('show', [ProfileController::class, 'show'])->name('show');
        $router->get('edit', [ProfileController::class, 'edit'])->name('edit');
        $router->put('edit', [ProfileController::class, 'update'])->name('update');
        $router->put('updatePassword', [ProfileController::class, 'updatePassword'])->name('updatePassword');
    });
//
//    /** Users */
    $router->resource('users', UserController::class);
    $router->put('users/{user}/updatePassword', [UserController::class, 'updatePassword'])->name('users.update_password');


    /** Groups */
    Route::resource('groups', GroupController::class);

    /** Group's Timetable */
    Route::resource('groups/{group}/timetables', TimetableController::class);

//    $router->resourse('groups', GroupController::class);
//    /** Articles */
//    $router->resource('articles', ArticlesController::class);
//
//    /** Pages */
//    $router->resource('pages', PageController::class, ['except' => ['store', 'create']]);
//    $router->post('pages/{page}/worker/add', [PageController::class, 'addWorker'])->name('pages.worker.add');
//    $router->post('pages/{page}/worker/{worker}/delete', [PageController::class, 'deleteWorker'])->name('pages.worker.delete');
//
//    /** Workers */
//    $router->resource('workers', WorkerController::class);
//
//    /** Orders */
//    $router->resource('orders', OrderController::class);

    /** Roles */
    $router->resource('roles', RoleController::class);
//
//    /** Dictionaries */
//    $router->resource('dictionaries', DictionaryController::class);
//
//    /** Dictionary Items */
//    $router->resource('dictionaries/{dictionary}/items', DictionaryItemController::class, ['names' => 'dictionary-items']);
//
//    /** Membership Applications */
//    $router->resource('applications', MembershipApplicationController::class, ['except' => ['store', 'create']]);
//    $router->put('applications/{application}/accept', [MembershipApplicationController::class, 'accept'])->name('application.accept');
});
