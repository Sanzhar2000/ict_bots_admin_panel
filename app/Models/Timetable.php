<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Timetable
 *
 * @property int $id
 * @property int $group_id
 * @property int $day_id
 * @property int $time_id
 * @property int $course_id
 * @property string $classroom
 * @property int $instructor_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Course $course
 * @property-read \App\Models\Day $day
 * @property-read \App\Models\Group $group
 * @property-read \App\Models\Instructor $instructor
 * @property-read \App\Models\Time $time
 * @method static \Illuminate\Database\Eloquent\Builder|Timetable newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Timetable newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Timetable query()
 * @method static \Illuminate\Database\Eloquent\Builder|Timetable whereClassroom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Timetable whereCourseId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Timetable whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Timetable whereDayId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Timetable whereGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Timetable whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Timetable whereInstructorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Timetable whereTimeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Timetable whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $type_id
 * @property-read \App\Models\Type $type
 * @method static \Illuminate\Database\Eloquent\Builder|Timetable whereTypeId($value)
 */
class Timetable extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'group_id',
        'day_id',
        'time_id',
        'course_id',
        'classroom',
        'type_id',
        'instructor_id'
    ];

    /* The relations to eager load on every query.
    *
    * @var array
    */
    protected $with = ['group', 'day', 'time', 'course', 'type', 'instructor'];

    public function group()
    {
        return $this->belongsTo(Group::class);
    }

    public function day()
    {
        return $this->belongsTo(Day::class);
    }

    public function time()
    {
        return $this->belongsTo(Time::class);
    }

    public function course()
    {
        return $this->belongsTo(Course::class);
    }

    public function type()
    {
        return $this->belongsTo(Type::class);
    }

    public function instructor()
    {
        return $this->belongsTo(Instructor::class);
    }
}
