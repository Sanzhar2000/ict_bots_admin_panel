<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Course;
use App\Models\Day;
use App\Models\Group;
use App\Models\Instructor;
use App\Models\Time;
use App\Models\Timetable;
use App\Models\Type;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;

class TimetableController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index(Group $group)
    {
        $timetables = $group->timetables;

        return view('admin.pages.timetables.index', compact('timetables','group'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View
     */
    public function create(Group $group)
    {
        $days = Day::all();
        $times = Time::all();
        $courses = Course::all();
        $types = Type::all();
        $instructors = Instructor::all();

        return view('admin.pages.timetables.create', compact(['group', 'days', 'times', 'courses', 'types', 'instructors']));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request, Group $group)
    {
        $this->validate($request, [
            'group_id' => 'required',
            'day_id' => 'required',
            'time_id' => 'required',
            'course_id' => 'required',
            'classroom' => 'required',
            'type_id' => 'required',
            'instructor_id' => 'required',
        ]);

        $timetable = Timetable::create($request->all());

        return redirect()->route('panel.timetables.show', [$group, $timetable])->with('success','Timetable created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param Timetable $timetable
     * @return Application|Factory|View
     */
    public function show(Group $group, Timetable $timetable)
    {
        $days = Day::all();
        return view('admin.pages.timetables.show', compact('timetable', 'group', 'days'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Timetable $timetable
     * @return Application|Factory|View
     */
    public function edit(Group $group, Timetable $timetable)
    {
        $days = Day::all();
        $times = Time::all();
        $courses = Course::all();
        $types = Type::all();
        $instructors = Instructor::all();

        return view('admin.pages.timetables.edit', compact(['group', 'timetable', 'days', 'times', 'courses', 'types', 'instructors']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Timetable $timetable
     * @return Application|RedirectResponse|Redirector
     */
    public function update(Request $request, Group $group, Timetable $timetable)
    {
        $timetable->update($request->all());

        return redirect(route('panel.timetables.show', [$group, $timetable]))->with('success','Timetable updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Timetable $timetable
     * @return Application|Redirector|RedirectResponse
     */
    public function destroy(Group $group, Timetable $timetable)
    {
        $timetable->forceDelete();

        return redirect(route('panel.timetables.index', $group))->with('success', 'Timetable deleted successfully');
    }
}
