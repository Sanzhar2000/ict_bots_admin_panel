<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Group;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;

class GroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        $groups = Group::paginate(10);

        return view('admin.pages.groups.index', compact('groups'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View
     */
    public function create()
    {
        return view('admin.pages.groups.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        $group = Group::create([
            'title' => $request->title
        ]);

        return redirect()->route('panel.groups.show', $group)->with('success','Group created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param Group $group
     * @return Application|Factory|View
     */
    public function show(Group $group)
    {
        return view('admin.pages.groups.show', compact('group'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Group $group
     * @return Application|Factory|View
     */
    public function edit(Group $group)
    {
        return view('admin.pages.groups.edit', compact('group'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Group $group
     * @return Application|Redirector|RedirectResponse
     */
    public function update(Request $request, Group $group)
    {
        $group->update($request->all());

        return redirect(route('panel.groups.show', $group))->with('success','Group updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Group $group
     * @return Application|Redirector|RedirectResponse
     */
    public function destroy(Group $group)
    {
        $group->forceDelete();

        return redirect( route('panel.groups.index'))->with('success', 'Group deleted successfully');
    }
}
