function TinyMceInit(selector, textOnly = false) {
    let lang = window.Laravel.lang,
        additionalTools = '', input, progressModal, progressBar, cancelUploadBtn, progressMsgEl;

    tinymce.init({
        selector: selector,
        menubar: false,
        allow_script_urls: true,
        plugins: [
            'lists link ' + additionalTools + ' table paste code wordcount'
        ],
        toolbar: 'undo redo | code | formatselect | ' +
            'bold italic link ' + additionalTools + ' | alignleft aligncenter ' +
            'alignright alignjustify | bullist numlist | ' +
            'removeformat | help',
        relative_urls: false,
        language: lang,
        setup: function (editor) {
            editor.on('change', function () {
                editor.save();
            });
            editor.on('Undo', function () {
                editor.save();
            });
            editor.on('Redo', function () {
                editor.save();
            });
        }
    });
}

(function () {
    /*init Tinymce*/
    if (document.querySelector('.tinymce-here')) {
        TinyMceInit('.tinymce-here');
    }
    if (document.querySelector('.tinymce-text-here')) {
        TinyMceInit('.tinymce-text-here', true);
    }
    /**/
})();
