@extends('admin.layouts.index')

@section('title', 'Пользователи')

@section('content')

<div class="container container-fluid">
    <div class="title-block">
        <div class="row row--multiline align-items-center">
            <div class="col-md-8">
                <ul class="breadcrumbs">
                    <li><a href="{{ route('panel.users.index') }}">Пользователи</a></li>
                    <li><span>{{$user->name}}</span></li>
                    <li><span>Редактировать</span></li>
                </ul>
            </div>
            <div class="col-md-4 text-right-md text-right-lg">
                <div class="flex-form">
                    <div class="pull-right">
{{--                        <a class="btn btn-primary" href="{{ route('panel.users.index') }}">Назад</a>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if($errors->hasBag('updateProfile'))
        <div class="alert alert-danger">
            @foreach($errors->login as $error)
                {{ $error }}<br>
            @endforeach
        </div>
    @endif

    @if(session()->has('updatePassword'))
        <div class="alert alert-danger">
            {{ session()->get('updatePassword') }}
        </div>
    @endif

    <form class="block" method="post" action="{{route('panel.users.update', $user) }}">
        @csrf
        @method('put')

        <div class="tabs">
            <div class="mobile-dropdown">
                <div class="mobile-dropdown__title dynamic">@lang('admin/pages.profile.main_info')</div>
                <div class="mobile-dropdown__desc">
                    <ul class="tabs-titles">
                        <li class="active"><a href="javascript:;" title="@lang('admin/pages.profile.main_info')">@lang('admin/pages.profile.main_info')</a></li>
                    </ul>
                </div>
            </div>
            <div class="tabs-contents">
                <div class="active">
                    <div class="input-group">
                        <label class="input-group__title"> @lang('admin/pages.profile.fullname')</label>
                        <input type="text" name="name" value="{{$user->name}}" placeholder="@lang('admin/pages.profile.fullname')" class="input-regular"
                               data-required-text=">@lang('admin/pages.profile.fullname_required')" required>
                    </div>
                    <br>
                    <div class="input-group">
                        <label class="input-group__title"> @lang('admin/pages.profile.email')</label>
                        <input type="email" name="email" value="{{$user->email}}" placeholder="@lang('admin/pages.profile.email')" class="input-regular"
                               data-required-text=">@lang('admin/pages.profile.email_required')" required>
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <div class="buttons">
            <div>
                <button type="submit" class="btn btn--green">@lang('admin/pages.profile.save')</button>
            </div>
        </div>
    </form>

    <form class="block" method="post" action="{{route('panel.users.update_password', $user) }}">
        @csrf
        @method('put')

        <div class="tabs">
            <div class="mobile-dropdown">
                <div class="mobile-dropdown__title dynamic">@lang('admin/pages.profile.update_password')</div>
                <div class="mobile-dropdown__desc">
                    <ul class="tabs-titles">
                        <li class="active"><a href="javascript:;" title="update_password">@lang('admin/pages.profile.update_password')</a></li>
                    </ul>
                </div>
            </div>

            <div class="tabs-contents">
                <div class="active">
                    <div class="input-group">
                        <label class="input-group__title"> @lang('admin/pages.profile.current_password')</label>
                        <input type="password" name="password" class="input-regular" placeholder="@lang('admin/pages.profile.current_password_placeholder')"
                               data-required-text="@lang('admin/pages.profile.current_password_required')" data-validate="password" required>
                    </div>
                    <br>
                    <div class="input-group">
                        <label class="input-group__title"> @lang('admin/pages.profile.new_password_confirmation')</label>
                        <input type="password" name="password_confirmation" class="input-regular" placeholder="@lang('admin/pages.profile.new_password_confirmation_placeholder')"
                               data-required-text="@lang('admin/pages.profile.password_confirmation_required')" data-validate="password_confirmation" required>
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <div class="buttons">
            <div>
                <button type="submit" class="btn btn--green">@lang('admin/pages.profile.save')</button>
            </div>
        </div>
    </form>
</div>

@endsection
@section('scripts')
    <!--Only this page's scripts-->
    <!---->
@endsection
