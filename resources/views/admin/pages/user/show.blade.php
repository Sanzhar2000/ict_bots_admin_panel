@extends('admin.layouts.index')

@section('title', 'Пользователи')

@section('content')
<div class="container container-fluid">
    <div class="title-block">
        <div class="row row--multiline align-items-center">
            <div class="col-md-8">
                <ul class="breadcrumbs">
                    <li><a href="{{ route('panel.users.index') }}">Пользователи</a></li>
                    <li><span>{{ $user->name }}</span></li>
                </ul>
            </div>
            <div class="col-md-4 text-right-md text-right-lg">
                <div class="flex-form">
                    <div class="pull-right">
                        <a class="btn btn-primary" href="{{ route('panel.users.index') }}">Назад</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if(session()->has('success'))
        <div class="alert alert-success">
            {{ session()->get('success') }}
        </div>
    @endif

    <div class="block">
        <h2 class="title-primary">Пользователь</h2>
        <div class="input-group">
            <label class="input-group__title">Имя:</label>
            <input type="text" name="name" placeholder="Название" class="input-regular" value="{{ $user->name }}" disabled>
        </div>
        <br>
        <div class="input-group">
            <label class="input-group__title">Почта:</label>
            <input type="email" name="email" placeholder="Название" class="input-regular" value="{{ $user->email }}" disabled>
        </div>
        <br>
        <div class="input-group">
            <label class="input-group__title">Роли:</label>
            @if(!empty($user->getRoleNames()))
                <div class="row">
                    @foreach($user->getRoleNames() as $name)
                        <div class="col-xs-1 col-md-2 col-sm-12 col-jg-1 ">
                            <input type="text" width="100"name="role" placeholder="Название" class="input-regular" value="{{ $name }}" disabled>
                        </div>
                    @endforeach
                </div>
            @endif
        </div>
        <br>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                @can('user-edit')
                    <a class="btn btn-primary" href="{{ route('panel.users.edit', $user->id) }}">Редактировать</a>
                @endcan
                @can('user-delete')
                    <a class="btn btn-danger btn--red" href="{{ route('panel.users.destroy', $user->id) }}"
                       onclick="event.preventDefault();
                                                     document.getElementById('delete-form').submit();" title="Удалить">Удалить
                    </a>
                    <form id="delete-form" action="{{ route('panel.users.destroy', $user->id) }}" method="POST" class="d-none">
                        @csrf
                        @method('DELETE')
                    </form>
                @endcan
            </div>
        </div>
    </div>
</div>

@endsection
@section('scripts')
    <!--Only this page's scripts-->
    <!---->
@endsection
