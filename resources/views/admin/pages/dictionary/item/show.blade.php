@extends('admin.layouts.index')

@section('title', 'Справочник')

@section('content')

<div class="container container-fluid">
    <div class="title-block">
        <div class="row row--multiline align-items-center">
            <div class="col-md-8">
                <ul class="breadcrumbs">
                    <li><span>Справочник</span></li>
                    <li><a href="{{ route('panel.dictionaries.show', $dictionary->id) }}">{{ $dictionary->name }}</a></li>
                    <li><span>{{ $item->name }}</span></li>
                </ul>
            </div>
            <div class="col-md-4 text-right-md text-right-lg">
                <div class="flex-form">
                    <div class="pull-right">
                        <a class="btn btn-primary" href="{{ route('panel.articles.index') }}">Назад</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <div class="block">
        <h2 class="title-primary">Словарь</h2>
        <div class="input-group">
            <label class="input-group__title">Название (РУС)</label>
            <input type="text" name="name_ru" placeholder="Название" class="input-regular" value="{{ $item->name_ru }}" disabled>
        </div>
        <br>
        <div class="input-group">
            <label class="input-group__title">Название (ҚАЗ)</label>
            <input type="text" name="name_kk" placeholder="Название" class="input-regular" value="{{ $item->name_kk }}" disabled>
        </div>
        <br>
        <div class="input-group">
            <label class="input-group__title">Название (ENG)</label>
            <input type="text" name="name_en" placeholder="Название" class="input-regular" value="{{ $item->name_en }}" disabled>
        </div>
        <br>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                @can('dictionary-item-edit')
                    <a class="btn btn-primary" href="{{ route('panel.dictionary-items.edit', [$dictionary, $item->id]) }}">Редактировать</a>
                @endcan
                @can('dictionary-item-delete')
                    <a class="btn btn-danger btn--red" href="{{ route('panel.dictionary-items.destroy', [$dictionary, $item->id ]) }}"
                       onclick="event.preventDefault();
                                                     document.getElementById('delete-form').submit();" title="Удалить">Удалить
                    </a>
                    <form id="delete-form" action="{{ route('panel.dictionary-items.destroy', [$dictionary, $item->id ]) }}" method="POST" class="d-none">
                        @csrf
                        @method('DELETE')
                    </form>
                @endcan
            </div>
        </div>
    </div>
</div>

@endsection
@section('scripts')
    <!--Only this page's scripts-->
    <!---->
@endsection
