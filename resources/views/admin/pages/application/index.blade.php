@extends('admin.layouts.index')

@section('title', 'Заявки на вступление в члены МСК')

@section('content')

<div class="container container-fluid">
    <div class="title-block">
        <div class="row row--multiline align-items-center">
            <div class="col-md-8">
                <h1 class="title-primary" style="margin-bottom: 0">Управление заявками на вступление в члены МСК</h1>
            </div>
            <div class="col-md-4 text-right-md text-right-lg">
                <div class="flex-form">
                    <div>
{{--                        @can('application-create')--}}
{{--                            <a class="btn btn-success" href="{{ route('panel.articles.create') }}">Добавить новость</a>--}}
{{--                        @endcan--}}
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <br>
    <div class="block">
        <h2 class="title-secondary">Список заявок</h2>
        <table class="table table-records">
            <colgroup>
                <col span="1" style="width: 3%;">
                <col span="1" style="width: 12%;">
                <col span="1" style="width: 10%;">
                <col span="1" style="width: 7%;">
                <col span="1" style="width: 7%;">
                <col span="1" style="width: 20%;">
                <col span="1" style="width: 10%;">
                <col span="1" style="width: 10%;">
                <col span="1" style="width: 10%;">
                <col span="1" style="width: 30%;">
            </colgroup>
            <thead>
                <tr>
                    <th>No</th>
                    <th>Названии организации</th>
                    <th>ФИО контактного лица</th>
                    <th>Электронный адрес</th>
                    <th>Номер телефона</th>
                    <th>Адрес</th>
                    <th>Площадь с/х угодий</th>
                    <th>Количество животных</th>
                    <th>Статус</th>
                    <th>Действие</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($applications as $application)
                    <tr>
                        <td>{{ $application->id }}</td>
                        <td>{{ $application->organization ?? '-' }}</td>
                        <td>{{ $application->name ?? '-' }}</td>
                        <td>{{ $application->email ?? '-' }}</td>
                        <td>{{ $application->phone ?? '-' }}</td>
                        <td>{{ $application->address ?? '-' }}</td>
                        <td>{{ $application->area ?? '-' }}</td>
                        <td>{{ $application->animals_count ?? '-' }}</td>
                        <td>{{ $application->accepted == 0 ? 'В ожидании' : 'Принят' ?? '-' }}</td>
                        <td>
                            <a class="icon-btn icon-btn--green icon-eye" href="{{ route('panel.applications.show', $application->id) }}" title="Смотреть"></a>
                            @can('application-edit')
                                <a class="icon-btn icon-btn--yellow icon-edit" href="{{ route('panel.applications.edit', $application->id) }}" title="Редактировать"></a>
                            @endcan
                            @can('application-delete')
                                <a href="javascript:;" title="Удалить"
                                   onclick="document.querySelector('#model-{{ $application->id }}').submit()"
                                   class="icon-btn icon-btn--pink icon-delete"></a>
                                <form action="{{ route('panel.applications.destroy',  $application->id) }}" id="model-{{ $application->id }}" method="post">
                                    @csrf
                                    @method('DELETE')
                                </form>
                            @endcan
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        {{ $applications->appends(\Illuminate\Support\Facades\Request::except('page'))->links("vendor.pagination.admin") }}
    </div>
</div>

@endsection
@section('scripts')
    <!--Only this page's scripts-->
    <!---->
@endsection
