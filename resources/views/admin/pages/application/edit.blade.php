@extends('admin.layouts.index')

@section('title', 'Заявки на вступление в члены МСК')

@section('content')

<div class="container container-fluid">
    <div class="title-block">
        <div class="row row--multiline align-items-center">
            <div class="col-md-8">
                <ul class="breadcrumbs">
                    <li><a href="{{ route('panel.applications.index') }}">Управление заявками на вступление в члены МСК</a></li>
                    <li><a href="{{ route('panel.applications.show', $application) }}">#{{ $application->id }}</a></li>
                    <li><span>Редактировать</span></li>
                </ul>
            </div>
            <div class="col-md-4 text-right-md text-right-lg">
                <div class="flex-form">
                    <div class="pull-right">
                        <a class="btn btn-primary" href="{{ route('panel.applications.index') }}">Назад</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="block">
        <form method="post" action="{{route('panel.applications.update', $application) }}">
            @csrf
            @method('put')
            <h2 class="title-primary">Заявка</h2>
            <div class="input-group">
                <div class="input-group__title">Названии организации</div>
                <input type="text" value="{{ $application->organization }}" placeholder="Названии организации" class="input-regular" name="organization" required>
            </div>
            <div class="input-group">
                <div class="input-group__title">ФИО контактного лица</div>
                <input type="text" value="{{ $application->name }}" placeholder="ФИО контактного лица" class="input-regular" name="name" required>
            </div>
            <div class="input-group">
                <label class="input-group__title">Электронный адрес</label>
                <input type="text" value="{{ $application->email }}" placeholder="Электронный адрес" class="input-regular" name="email" required>
            </div>
            <div class="input-group">
                <div class="input-group__title">Номер телефона</div>
                <input type="text" value="{{ $application->phone }}" placeholder="Номер телефона" onfocus="$(this).inputmask('+7 (999) 999 99 99');" class="input-regular" name="phone" required>
            </div>
            <div class="input-group">
                <div class="input-group__title">Адрес</div>
                <input type="text" value="{{ $application->address }}" placeholder="Адрес" class="input-regular" name="address" required>
            </div>
            <div class="input-group">
                <label class="input-group__title">Площадь с/х угодий</label>
                <input type="text" value="{{ $application->area }}" min="1" placeholder="Площадь с/х угодий" class="input-regular" name="area" required>
            </div>
            <div class="input-group">
                <label class="input-group__title">Количество животных</label>
                <input type="text" value="{{ $application->animals_count }}" min="1" placeholder="Количество животных" class="input-regular" name="animals_count" required>
            </div>
            <br>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                    <button type="submit" class="btn btn--green">Сохранить</button>
                    @can('worker-delete')
                        <a href="javascript:;" title="Одобрить"
                           onclick="document.querySelector('#model-accept-{{ $application->id }}').submit()"
                           class="btn btn-">Одобрить</a>
                        <a class="btn btn-danger btn--red" href="{{ route('panel.applications.destroy', $application->id) }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('delete-form').submit();" title="Удалить">Удалить
                        </a>
                    @endcan
                </div>
            </div>
        </form>
        <br>
        @can('application-delete')
            <form id="delete-form" action="{{ route('panel.applications.destroy', $application->id) }}" method="POST" class="d-none">
                @csrf
                @method('DELETE')
            </form>
            <form action="{{ route('panel.application.accept', $application->id) }}" id="model-accept-{{ $application->id }}" method="post">
                @csrf
                @method('put')
            </form>
        @endcan
    </div>
</div>

@endsection
@section('scripts')
    <!--Only this page's scripts-->
    <script src="https://cdn.tiny.cloud/1/eutvjr9zyhc4qtchwyfwhutknw2iunsf80kiuye2fdomu2wd/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script src="/admin/js/api.js"></script>
    <script src="/admin/js/tinymce.js"></script>
    <!---->
@endsection
