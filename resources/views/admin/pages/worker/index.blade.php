@extends('admin.layouts.index')

@section('title', 'Сотрудники')

@section('content')

<div class="container container-fluid">
    <div class="title-block">
        <div class="row row--multiline align-items-center">
            <div class="col-md-8">
                <h1 class="title-primary" style="margin-bottom: 0">Управление сотрудниками</h1>
            </div>
            <div class="col-md-4 text-right-md text-right-lg">
                <div class="flex-form">
                    <div>
                        @can('worker-create')
                            <a class="btn btn-success" href="{{ route('panel.workers.create') }}">Добавить сотрудника</a>
                        @endcan
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <br>
    <div class="block">
        <h2 class="title-secondary">Список новостей</h2>
        <table class="table table-records">
            <colgroup>
                <col span="1" style="width: 3%;">
                <col span="1" style="width: 20%;">
                <col span="1" style="width: 30%;">
                <col span="1" style="width: 30%;">
            </colgroup>
            <thead>
                <tr>
                    <th>No</th>
                    <th>ФИО</th>
                    <th>Должность</th>
                    <th>Действие</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($workers as $worker)
                    <tr>
                        <td>{{ $worker->id }}</td>
                        <td>{{ $worker->name_ru }}</td>
                        <td>{{ $worker->position_ru }}</td>
                        <td>
                            <a class="icon-btn icon-btn--green icon-eye" href="{{ route('panel.workers.show', $worker->id) }}" title="Смотреть"></a>
                            @can('worker-edit')
                                <a class="icon-btn icon-btn--yellow icon-edit" href="{{ route('panel.workers.edit', $worker->id) }}" title="Редактировать"></a>
                            @endcan
                            @can('worker-delete')
                                <a href="javascript:;" title="Удалить"
                                   onclick="document.querySelector('#model-{{ $worker->id }}').submit()"
                                   class="icon-btn icon-btn--pink icon-delete"></a>
                                <form action="{{ route('panel.workers.destroy',  $worker->id) }}" id="model-{{ $worker->id }}" method="post">
                                    @csrf
                                    @method('DELETE')
                                </form>
                            @endcan
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        {{ $workers->appends(\Illuminate\Support\Facades\Request::except('page'))->links("vendor.pagination.admin") }}
    </div>
</div>

@endsection
@section('scripts')
    <!--Only this page's scripts-->
    <!---->
@endsection
