@extends('admin.layouts.index')

@section('title', 'Сотрудники')

@section('content')

<div class="container container-fluid">
    <div class="title-block">
        <div class="row row--multiline align-items-center">
            <div class="col-md-8">
                <ul class="breadcrumbs">
                    <li><a href="{{ route('panel.workers.index') }}">Сотрудники</a></li>
                    <li><span>{{ $worker->name }}</span></li>
                </ul>
            </div>
            <div class="col-md-4 text-right-md text-right-lg">
                <div class="flex-form">
                    <div class="pull-right">
                        <a class="btn btn-primary" href="{{ route('panel.workers.index') }}">Назад</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <div class="block">
        <h2 class="title-primary">Сотрудник</h2>
        <div class="input-group">
            <div class="input-group__title">Фотограция</div>
            <a href="{{ $worker->image }}" target="_blank">
                <img class="article-image" src="{{ $worker->image ?? ''}}" height="200" width="400">
            </a>
        </div>
        <div class="input-group">
            <label class="input-group__title">ФИО (РУС)</label>
            <input type="text" name="name_ru" placeholder="Название" class="input-regular" value="{{ $worker->name_ru }}" disabled>
        </div>
        <br>
        <div class="input-group">
            <label class="input-group__title">Должность (РУС)</label>
            <input type="text" name="position_ru" placeholder="Должность" class="input-regular" value="{{ $worker->position_ru }}" disabled>
        </div>
        <br>
        <div class="input-group">
            <label class="input-group__title">ТАӘ (ҚАЗ)</label>
            <input type="text" name="name_kk" placeholder="Название" class="input-regular" value="{{ $worker->name_kk }}" disabled>
        </div>
        <br>
        <div class="input-group">
            <label class="input-group__title">Лауазым (ҚАЗ)</label>
            <input type="text" name="position_kk" placeholder="Лауазым" class="input-regular" value="{{ $worker->position_kk }}" disabled>
        </div>
        <br>
        <div class="input-group">
            <label class="input-group__title">Full name (ENG)</label>
            <input type="text" name="name_en" placeholder="Название" class="input-regular" value="{{ $worker->name_en }}" disabled>
        </div>
        <br>
        <div class="input-group">
            <label class="input-group__title">Position (ENG)</label>
            <input type="text" name="position_en" placeholder="Position" class="input-regular" value="{{ $worker->position_en }}" disabled>
        </div>
        <br>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                @can('worker-edit')
                    <a class="btn btn-primary" href="{{ route('panel.workers.edit', $worker->id) }}">Редактировать</a>
                @endcan
                @can('worker-delete')
                    <a class="btn btn-danger btn--red" href="{{ route('panel.workers.destroy', $worker->id) }}"
                       onclick="event.preventDefault();
                                                     document.getElementById('delete-form').submit();" title="Удалить"">Удалить
                    </a>
                    <form id="delete-form" action="{{ route('panel.workers.destroy', $worker->id) }}" method="POST" class="d-none">
                        @csrf
                        @method('DELETE')
                    </form>
                @endcan
            </div>
        </div>
    </div>
</div>

@endsection
@section('scripts')
    <!--Only this page's scripts-->
    <!---->
@endsection
