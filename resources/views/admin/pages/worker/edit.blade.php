@extends('admin.layouts.index')

@section('title', 'Сотрудники')

@section('content')

<div class="container container-fluid">
    <div class="title-block">
        <div class="row row--multiline align-items-center">
            <div class="col-md-8">
                <ul class="breadcrumbs">
                    <li><a href="{{ route('panel.workers.index') }}">Соторудники</a></li>
                    <li><a href="{{ route('panel.workers.show', $worker->id) }}">{{ $worker->name }}</a></li>
                    <li><span>{{ $worker->name }}</span></li>
                    <li><span>Редактировать</span></li>
                </ul>
            </div>
            <div class="col-md-4 text-right-md text-right-lg">
                <div class="flex-form">
                    <div class="pull-right">
                        <a class="btn btn-primary" href="{{ route('panel.dictionaries.index') }}">Назад</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="block">
        <form method="post" action="{{route('panel.workers.update', $worker) }}" enctype="multipart/form-data">
            @csrf
            @method('put')
            <h2 class="title-primary">Словарь</h2>
            <div class="input-group">
                <label class="input-group__title">Название (РУС)<span class="required">*</span></label>
                <input type="text" name="name_ru" placeholder="Название" class="input-regular" value="{{ $worker->name_ru }}" required>
            </div>
            <br>
            <div class="input-group">
                <label class="input-group__title">Должность (РУС)<span class="required">*</span></label>
                <input type="text" name="position_ru" placeholder="Название" class="input-regular" value="{{ $worker->position_ru }}"required>
            </div>
            <br>
            <div class="input-group">
                <label class="input-group__title">Название (ҚАЗ)<span class="required">*</span></label>
                <input type="text" name="name_kk" placeholder="Название" class="input-regular" value="{{ $worker->name_kk }}" required>
            </div>
            <br>
            <div class="input-group">
                <label class="input-group__title">Лауазымы (ҚАЗ)<span class="required">*</span></label>
                <input type="text" name="position_kk" placeholder="Название" class="input-regular" value="{{ $worker->position_kk }}" required>
            </div>
            <br>
            <div class="input-group">
                <label class="input-group__title">Название (ENG)<span class="required">*</span></label>
                <input type="text" name="name_en" placeholder="Название" class="input-regular" value="{{ $worker->name_en }}" required>
            </div>
            <br>
            <div class="input-group">
                <label class="input-group__title">Position (ENG)<span class="required">*</span></label>
                <input type="text" name="position_en" placeholder="Название" class="input-regular" value="{{ $worker->position_en }}"required>
            </div>
            <br>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <label class="input-group__title">Фото</label>
                    <input type="file" name="image" class="form-control" placeholder="image">
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                    <button type="submit" class="btn btn--green">Сохранить</button>
                    @can('worker-delete')
                        <a class="btn btn-danger btn--red" href="{{ route('panel.workers.destroy', $worker->id) }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('delete-form').submit();" title="Удалить">Удалить
                        </a>
                    @endcan
                </div>
            </div>
        </form>
        @can('order-delete')
            <form id="delete-form" action="{{ route('panel.workers.destroy', $worker->id) }}" method="POST" class="d-none">
                @csrf
                @method('DELETE')
            </form>
        @endcan
    </div>
</div>

@endsection
@section('scripts')
    <!--Only this page's scripts-->
    <!---->
@endsection
