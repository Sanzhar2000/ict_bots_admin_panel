@extends('admin.layouts.index')

@section('title', 'Новости')

@section('content')

<div class="container container-fluid">
    <div class="title-block">
        <div class="row row--multiline align-items-center">
            <div class="col-md-8">
                <ul class="breadcrumbs">
                    <li><a href="{{ route('panel.timetables.index', $group) }}">Timetables of {{ $group->title }}</a></li>
                </ul>
            </div>
            <div class="col-md-4 text-right-md text-right-lg">
                <div class="flex-form">
                    <div class="pull-right">
                        <a class="btn btn-primary" href="{{ route('panel.timetables.index', $group) }}">Back</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="block">
        @can('record-create')
        <form method="post" action="{{route('panel.timetables.store', $group) }}" enctype="multipart/form-data">
            @csrf
            <h2 class="title-primary">Timetable</h2>

            <input name="group_id" type="hidden" value="{{ $group->id ?? "-" }}">

            <div class="input-group">
                <div class="input-group">
                    <label class="input-group__title">Day</label>
                    <select class="input-regular" name="day_id">
                        <option value="">{{"-"}}</option>
                        @foreach($days as $day)
                            <option value="{{ $day->id }}">{{$day->title}}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="input-group">
                <div class="input-group">
                    <label class="input-group__title">Time</label>
                    <select class="input-regular" name="time_id">
                        <option value="">{{"-"}}</option>
                        @foreach($times as $time)
                            <option value="{{ $time->id }}">{{$time->title}}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="input-group">
                <div class="input-group">
                    <label class="input-group__title">Course</label>
                    <select class="input-regular" name="course_id">
                        <option value="">{{"-"}}</option>
                        @foreach($courses as $course)
                            <option value="{{ $course->id }}">{{$course->title}}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="input-group">
                <label class="input-group__title">Classroom</label>
                <input type="text" name="classroom" title="Опубликован" class="input-regular">
            </div>

            <div class="input-group">
                <div class="input-group">
                    <label class="input-group__title">Type</label>
                    <select class="input-regular" name="type_id">
                        <option value="">{{"-"}}</option>
                        @foreach($types as $type)
                            <option value="{{ $type->id }}">{{$type->title}}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="input-group">
                <div class="input-group">
                    <label class="input-group__title">Instructor</label>
                    <select class="input-regular" name="instructor_id">
                        <option value="">{{"-"}}</option>
                        @foreach($instructors as $instructor)
                            <option value="{{ $instructor->id }}">{{$instructor->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Сохранить</button>
            </div>
        </form>
        @endcan
    </div>
</div>

@endsection
@section('scripts')
    <!--Only this page's scripts-->
        <script src="https://cdn.tiny.cloud/1/eutvjr9zyhc4qtchwyfwhutknw2iunsf80kiuye2fdomu2wd/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
        <script src="/admin/js/api.js"></script>
        <script src="/admin/js/tinymce.js"></script>
    <!---->
@endsection
