@extends('admin.layouts.index')

@section('title', 'Новости')

@section('content')

<div class="container container-fluid">
    <div class="title-block">
        <div class="row row--multiline align-items-center">
            <div class="col-md-8">
                <h1 class="title-primary" style="margin-bottom: 0">Timetable of {{ $group->title }}</h1>
            </div>
            <div class="col-md-4 text-right-md text-right-lg">
                <div class="flex-form">
                    <div>
                        @can('record-create')
                            <a class="btn btn-success" href="{{ route('panel.timetables.create', $group->id) }}">Add lessons</a>
                        @endcan
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <br>
    <div class="block">
        <h2 class="title-secondary">Список новостей</h2>
        <table class="table table-records">
            <colgroup>
                <col span="1" style="width: 3%;">
                <col span="1" style="width: 14%;">
                <col span="1" style="width: 18%;">
                <col span="1" style="width: 10%;">
                <col span="1" style="width: 13%;">
                <col span="1" style="width: 14%;">
                <col span="1" style="width: 10%;">
                <colgroup span="1" style="width: 18%;">
            </colgroup>
            <thead>
                <tr>
                    <th>No</th>
                    <th>Day</th>
                    <th>Time</th>
                    <th>Course</th>
                    <th>Classroom</th>
                    <th>Type</th>
                    <th>Instructor</th>
                    <th>Действия</th>
                </tr>
            </thead>
            <tbody>
            <tbody>
                @foreach ($timetables as $timetable)
                    <tr>
                        <td>{{ $timetable->id }}</td>
                        <td>{{ $timetable->day->title }}</td>
                        <td>{{ $timetable->time->title }}</td>
                        <td>{{ $timetable->course->title }}</td>
                        <td>{{ $timetable->classroom }}</td>
                        <td>{{ $timetable->type->title }}</td>
                        <td>{{ $timetable->instructor->name }}</td>
                        <td>
                            <a class="icon-btn icon-btn--green icon-eye" href="{{ route('panel.timetables.show', [$group->id, $timetable->id]) }}" title="Смотреть"></a>
                            @can('record-edit')
                                <a class="icon-btn icon-btn--yellow icon-edit" href="{{ route('panel.timetables.edit', [$group->id, $timetable->id]) }}" title="Редактировать"></a>
                            @endcan
                            @can('record-delete')
                                <a href="javascript:;" title="Удалить"
                                   onclick="document.querySelector('#model-{{ $timetable->id }}').submit()"
                                   class="icon-btn icon-btn--pink icon-delete"></a>
                                <form action="{{ route('panel.timetables.destroy',  [$group, $timetable]) }}" id="model-{{ $timetable->id }}" method="post">
                                    @csrf
                                    @method('DELETE')
                                </form>
                            @endcan
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

{{--        {{ $timetables->appends(\Illuminate\Support\Facades\Request::except('page'))->links("vendor.pagination.admin") }}--}}
    </div>
</div>

@endsection
@section('scripts')
    <!--Only this page's scripts-->
    <!---->
@endsection
