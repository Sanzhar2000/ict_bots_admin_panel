@extends('admin.layouts.index')

@section('title', 'Новости')

@section('content')

<div class="container container-fluid">
    <div class="title-block">
        <div class="row row--multiline align-items-center">
            <div class="col-md-8">
                <ul class="breadcrumbs">
                    <li><a href="{{ route('panel.timetables.index', $group) }}">Timetables of {{ $group->title }}</a></li>
                    <li><a href="{{ route('panel.timetables.show', [$group, $timetable]) }}">{{ $timetable->title }}</a></li>
                </ul>
            </div>
            <div class="col-md-4 text-right-md text-right-lg">
                <div class="flex-form">
                    <div class="pull-right">
                        <a class="btn btn-primary" href="{{ route('panel.timetables.index', $group, $timetable) }}">Back</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="block">
        <form method="post" action="{{route('panel.timetables.update', [$group, $timetable]) }}" enctype="multipart/form-data">
            @csrf
            @method('put')

            <h2 class="title-primary">Timetable</h2>

            <div class="input-group">
                <div class="input-group">
                    <label class="input-group__title">Day</label>
                    <select class="input-regular" name="day_id">
                        @foreach($days as $day)
                            @if($day->id == $timetable->day->id)
                                <option selected value="{{ $day->id }}">{{$day->title}}</option>
                            @else
                                <option value="{{ $day->id }}">{{$day->title}}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="input-group">
                <div class="input-group">
                    <label class="input-group__title">Time</label>
                    <select class="input-regular" name="time_id">
                        @foreach($times as $time)
                            @if($time->id == $timetable->time->id)
                                <option selected value="{{ $time->id }}">{{$time->title}}</option>
                            @else
                                <option value="{{ $time->id }}">{{$time->title}}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="input-group">
                <div class="input-group">
                    <label class="input-group__title">Course</label>
                    <select class="input-regular" name="course_id">
                        @foreach($courses as $course)
                            @if($course->id == $timetable->course->id)
                                <option selected value="{{ $course->id }}">{{$course->title}}</option>
                            @else
                                <option value="{{ $course->id }}">{{$course->title}}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="input-group">
                <label class="input-group__title">Classroom</label>
                <input type="text" name="classroom" title="Опубликован" class="input-regular"
                       value="{{ $timetable->classroom ?? '-' }}">
            </div>

            <div class="input-group">
                <div class="input-group">
                    <label class="input-group__title">Type</label>
                    <select class="input-regular" name="type_id">
                        @foreach($types as $type)
                            @if($type->id == $timetable->type->id)
                                <option selected value="{{ $type->id }}">{{$type->title}}</option>
                            @else
                                <option value="{{ $type->id }}">{{$type->title}}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="input-group">
                <div class="input-group">
                    <label class="input-group__title">Instructor</label>
                    <select class="input-regular" name="instructor_id">
                        @foreach($instructors as $instructor)
                            @if($instructor->id == $timetable->instructor->id)
                                <option selected value="{{ $instructor->id }}">{{$instructor->name}}</option>
                            @else
                                <option value="{{ $instructor->id }}">{{$instructor->name}}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="input-group">
                <label class="input-group__title">Date of publishing</label>
                <input type="text" name="created_at" title="Опубликован" class="input-regular"
                       value="{{ $timetable->created_at ?? '-' }}">
            </div>


            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                    <button type="submit" class="btn btn--green">Save</button>
                    @can('record-delete')
                        <a class="btn btn-danger btn--red" href="{{ route('panel.timetables.destroy', [$group, $timetable]) }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('delete-form').submit();" title="Удалить">Delete
                        </a>
                    @endcan
                </div>
            </div>
        </form>
        @can('record-delete')
            <form id="delete-form" action="{{ route('panel.timetables.destroy', [$group, $timetable]) }}" method="POST" class="d-none">
                @csrf
                @method('DELETE')
            </form>
        @endcan
    </div>
</div>

@endsection
@section('scripts')
    <!--Only this page's scripts-->
    <script src="https://cdn.tiny.cloud/1/eutvjr9zyhc4qtchwyfwhutknw2iunsf80kiuye2fdomu2wd/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script src="/admin/assets/api.js"></script>
    <script src="/admin/assets/js/tinymce.js"></script>
    <!---->
@endsection
