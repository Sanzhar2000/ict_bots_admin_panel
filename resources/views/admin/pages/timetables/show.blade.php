@extends('admin.layouts.index')

@section('title', 'Новости')

@section('content')

<div class="container container-fluid">
    <div class="title-block">
        <div class="row row--multiline align-items-center">
            <div class="col-md-8">
                <ul class="breadcrumbs">
                    <li><a href="{{ route('panel.timetables.index', $group->id) }}">Lessons</a></li>
                    <li><span>{{ $group->title }}</span></li>
                </ul>
            </div>
            <div class="col-md-4 text-right-md text-right-lg">
                <div class="flex-form">
                    <div class="pull-right">
                        <a class="btn btn-primary btn--green" href="{{ route('panel.timetables.index', $group->id) }}">Timetable</a>
                    </div>

                    <div class="pull-right">
                        <a class="btn btn-primary" href="{{ route('panel.timetables.index', $group->id) }}">Back</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <div class="block">
        <h2 class="title-primary">Timetable</h2>

        <div class="input-group">
            <div class="input-group">
                <label class="input-group__title">Day</label>
                <select class="input-regular" name="day_id" disabled>
                    <option value="{{ $timetable->day->id }}">{{ $timetable->day->title }}</option>
                </select>
            </div>
        </div>

        <div class="input-group">
            <div class="input-group">
                <label class="input-group__title">Time</label>
                <select class="input-regular" name="time_id" disabled>
                    <option value="{{ $timetable->time->id }}">{{ $timetable->time->title }}</option>
                </select>
            </div>
        </div>

        <div class="input-group">
            <div class="input-group">
                <label class="input-group__title">Course</label>
                <select class="input-regular" name="course_id" disabled>
                    <option value="{{ $timetable->course->id }}">{{ $timetable->course->title }}</option>
                </select>
            </div>
        </div>

        <div class="input-group">
            <label class="input-group__title">Classroom</label>
            <input type="text" name="classroom" title="Опубликован" class="input-regular"
                   value="{{ $timetable->classroom ?? '-' }}" disabled>
        </div>

        <div class="input-group">
            <div class="input-group">
                <label class="input-group__title">Type</label>
                <select class="input-regular" name="type_id" disabled>
                    <option value="{{ $timetable->type->id }}">{{ $timetable->type->title }}</option>
                </select>
            </div>
        </div>

        <div class="input-group">
            <div class="input-group">
                <label class="input-group__title">Instructor</label>
                <select class="input-regular" name="instructor_id" disabled>
                    <option value="{{ $timetable->instructor->id }}">{{$timetable->instructor->name}}</option>
                </select>
            </div>
        </div>

        <div class="input-group">
            <label class="input-group__title">Date of publishing</label>
            <input type="text" name="created_at" title="Опубликован" class="input-regular"
                   value="{{ $timetable->created_at ?? '-' }}" disabled>
        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                @can('record-edit')
                    <a class="btn btn-primary" href="{{ route('panel.timetables.edit', [$group->id, $timetable->id]) }}">Редактировать</a>
                @endcan
                @can('record-delete')
                    <a class="btn btn-danger btn--red" href="{{ route('panel.timetables.destroy', [$group->id, $timetable->id]) }}"
                       onclick="event.preventDefault();
                                                     document.getElementById('delete-form').submit();" title="Удалить">Удалить
                    </a>
                    <form id="delete-form" action="{{ route('panel.timetables.destroy', [$group->id, $timetable->id]) }}" method="POST" class="d-none">
                        @csrf
                        @method('DELETE')
                    </form>
                @endcan
            </div>
        </div>
    </div>
</div>

@endsection
@section('scripts')
    <!--Only this page's scripts-->
    <!---->
@endsection
