@extends('admin.layouts.index')

@section('title', 'Страницы')

@section('content')

<div class="container container-fluid">
    <div class="title-block">
        <div class="row row--multiline align-items-center">
            <div class="col-md-8">
                <ul class="breadcrumbs">
                    <li><a href="{{ route('panel.pages.index') }}">Управление страницами</a></li>
                    <li><a href="{{ route('panel.pages.show', $page) }}">{{ $page->title }}</a></li>
                    <li><span>Редактировать</span></li>
                </ul>
            </div>
            <div class="col-md-4 text-right-md text-right-lg">
                <div class="flex-form">
                    <div class="pull-right">
                        <a class="btn btn-primary" href="{{ route('panel.pages.index') }}">Назад</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="block">
        <form method="post" action="{{route('panel.pages.update', $page) }}" enctype="multipart/form-data">
            @csrf
            @method('put')

            <h2 class="title-primary">Новость</h2>
            <div class="tabs">
                <div class="mobile-dropdown">
                    <div class="mobile-dropdown__title dynamic">Рус</div>
                    <div class="mobile-dropdown__desc">
                        <ul class="tabs-titles">
                            <li class="active"><a href="javascript:;" title="Рус">Рус</a></li>
                            <li><a href="javascript:;" id="" title="Қаз">Қаз</a></li>
                            <li><a href="javascript:;" id="" title="Қаз">Eng</a></li>
                        </ul>
                    </div>
                </div>
                <div class="tabs-contents">
                    <div class="active">
                        <div class="input-group">
                            <label class="input-group__title">Название <span class="required">*</span></label>
                            <input type="text" name="title_ru" placeholder="Название" class="input-regular" value="{{ $page->title_ru }}">
                        </div>
                        <br>
                        <div class="input-group">
                            <div class="input-group__title">Описание <span class="required">*</span></div>
                            <textarea name="description_ru" class="tinymce-here input-regular" >{!! $page->description_ru !!}</textarea>
                        </div>
                        <br>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <label class="input-group__title">Фото</label>
                                <input type="file" name="image_ru" class="form-control" placeholder="image">
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="input-group">
                            <label class="input-group__title">Атауы <span class="required">*</span></label>
                            <input type="text" name="title_kk" placeholder="Название" class="input-regular" value="{{ $page->title_kk }}">
                        </div>
                        <br>
                        <div class="input-group">
                            <div class="input-group__title">Сипаттамасы <span class="required">*</span></div>
                            <textarea name="description_kk" class="tinymce-here input-regular">{!! $page->description_kk !!}</textarea>
                        </div>
                        <br>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <label class="input-group__title">Фото</label>
                                <input type="file" name="image_kk" class="form-control" placeholder="image">
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="input-group">
                            <label class="input-group__title">Title <span class="required">*</span></label>
                            <input type="text" name="title_en" placeholder="Название" class="input-regular" value="{{ $page->title_en }}">
                        </div>
                        <br>
                        <div class="input-group">
                            <div class="input-group__title">Description <span class="required">*</span></div>
                            <textarea name="description_en" class="tinymce-here input-regular">{!! $page->description_en !!}</textarea>
                        </div>
                        <br>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <label class="input-group__title">Image</label>
                                <input type="file" name="image_en" class="form-control" placeholder="image">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                    <button type="submit" class="btn btn--green">Сохранить</button>
{{--                    @can('page-delete')--}}
{{--                        <a class="btn btn-danger btn--red" href="{{ route('panel.pages.destroy', $page->id) }}"--}}
{{--                           onclick="event.preventDefault();--}}
{{--                                                         document.getElementById('delete-form').submit();" title="Удалить">Удалить--}}
{{--                        </a>--}}
{{--                    @endcan--}}
                </div>
            </div>
        </form>
        <br>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
{{--            @can('page-delete')--}}
{{--                <form id="delete-form" action="{{ route('panel.pages.destroy', $page->id) }}" method="POST" class="d-none">--}}
{{--                    @csrf--}}
{{--                    @method('DELETE')--}}
{{--                </form>--}}
{{--            @endcan--}}
            </div>
        </div>
    </div>
</div>

@endsection
@section('scripts')
    <!--Only this page's scripts-->
    <script src="https://cdn.tiny.cloud/1/eutvjr9zyhc4qtchwyfwhutknw2iunsf80kiuye2fdomu2wd/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script src="/admin/js/api.js"></script>
    <script src="/admin/js/tinymce.js"></script>
    <!---->
@endsection
