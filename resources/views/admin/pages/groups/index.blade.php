@extends('admin.layouts.index')

@section('title', 'Новости')

@section('content')

<div class="container container-fluid">
    <div class="title-block">
        <div class="row row--multiline align-items-center">
            <div class="col-md-8">
                <h1 class="title-primary" style="margin-bottom: 0">Groups</h1>
            </div>
            <div class="col-md-4 text-right-md text-right-lg">
                <div class="flex-form">
                    <div>
                        @can('record-create')
                            <a class="btn btn-success" href="{{ route('panel.groups.create') }}">Add group</a>
                        @endcan
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <br>
    <div class="block">
        <h2 class="title-secondary">Список новостей</h2>
        <table class="table table-records">
            <colgroup>
                <col span="1" style="width: 3%;">
                <col span="1" style="width: 40%;">
                <col span="1" style="width: 17.5%;">
                <col span="1" style="width: 17.5%;">
            </colgroup>
            <thead>
                <tr>
                    <th>No</th>
                    <th>Название</th>
                    <th>Опубликован</th>
                    <th>Дата публикации</th>
                    <th>Действия</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($groups as $group)
                    <tr>
                        <td>{{ $group->id }}</td>
                        <td>{{ $group->title }}</td>
                        <td>{{ $group->created_at ?? '-'}}</td>
                        <td>{{ $group->updated_at ?? '-' }}</td>
                        <td>
                            <a class="icon-btn icon-btn--green icon-eye" href="{{ route('panel.groups.show', $group->id) }}" title="Смотреть"></a>
                            @can('record-edit')
                                <a class="icon-btn icon-btn--yellow icon-edit" href="{{ route('panel.groups.edit', $group->id) }}" title="Редактировать"></a>
                            @endcan
                            @can('record-delete')
                                <a href="javascript:;" title="Удалить"
                                   onclick="document.querySelector('#model-{{ $group->id }}').submit()"
                                   class="icon-btn icon-btn--pink icon-delete"></a>
                                <form action="{{ route('panel.groups.destroy',  $group->id) }}" id="model-{{ $group->id }}" method="post">
                                    @csrf
                                    @method('DELETE')
                                </form>
                            @endcan
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        {{ $groups->appends(\Illuminate\Support\Facades\Request::except('page'))->links("vendor.pagination.admin") }}
    </div>
</div>

@endsection
@section('scripts')
    <!--Only this page's scripts-->
    <!---->
@endsection
