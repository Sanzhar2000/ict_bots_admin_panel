@extends('admin.layouts.index')

@section('title', 'Новости')

@section('content')

<div class="container container-fluid">
    <div class="title-block">
        <div class="row row--multiline align-items-center">
            <div class="col-md-8">
                <ul class="breadcrumbs">
                    <li><a href="{{ route('panel.groups.index') }}">Groups</a></li>
                    <li><span>{{ $group->title }}</span></li>
                </ul>
            </div>
            <div class="col-md-4 text-right-md text-right-lg">
                <div class="flex-form">
                    <div class="pull-right">
                        <a class="btn btn-primary btn--green" href="{{ route('panel.timetables.index', $group->id) }}">Timetable</a>
                    </div>

                    <div class="pull-right">
                        <a class="btn btn-primary" href="{{ route('panel.groups.index') }}">Back</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <div class="block">
        <h2 class="title-primary">Group</h2>
        <div class="tabs">
            <div class="tabs-contents">
                <div class="active">
                    <div class="input-group">
                        <label class="input-group__title">Title</label>
                        <input type="text" name="title_ru" placeholder="Title"
                               class="input-regular" value="{{ $group->title }}" disabled>
                    </div>
                </div>
            </div>
        </div>
        <div class="input-group">
            <label class="input-group__title">Date of publishing</label>
            <input type="text" name="published_at" title="Опубликован" class="input-regular"
                   value="{{ $group->created_at ?? '-' }}" disabled>
        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                @can('record-edit')
                    <a class="btn btn-primary" href="{{ route('panel.groups.edit', $group->id) }}">Редактировать</a>
                @endcan
                @can('record-delete')
                    <a class="btn btn-danger btn--red" href="{{ route('panel.groups.destroy', $group->id) }}"
                       onclick="event.preventDefault();
                                                     document.getElementById('delete-form').submit();" title="Удалить">Удалить
                    </a>
                    <form id="delete-form" action="{{ route('panel.groups.destroy', $group->id) }}" method="POST" class="d-none">
                        @csrf
                        @method('DELETE')
                    </form>
                @endcan
            </div>
        </div>
    </div>
</div>

@endsection
@section('scripts')
    <!--Only this page's scripts-->
    <!---->
@endsection
