@extends('admin.layouts.index')

@section('title', 'Новости')

@section('content')

<div class="container container-fluid">
    <div class="title-block">
        <div class="row row--multiline align-items-center">
            <div class="col-md-8">
                <ul class="breadcrumbs">
                    <li><a href="{{ route('panel.groups.index') }}">Groups</a></li>
                    <li><a href="{{ route('panel.groups.show', $group) }}">{{ $group->title }}</a></li>
                    <li><span>Редактировать</span></li>
                </ul>
            </div>
            <div class="col-md-4 text-right-md text-right-lg">
                <div class="flex-form">
                    <div class="pull-right">
                        <a class="btn btn-primary" href="{{ route('panel.groups.index') }}">Back</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="block">
        <form method="post" action="{{route('panel.groups.update', $group) }}" enctype="multipart/form-data">
            @csrf
            @method('put')


            <h2 class="title-primary">Group</h2>
            <div class="tabs">
                <div class="tabs-contents">
                    <div class="active">
                        <div class="input-group">
                            <label class="input-group__title">Title</label>
                            <input type="text" name="title" placeholder="Title"
                                   class="input-regular" value="{{ $group->title }}">
                        </div>
                    </div>
                </div>
            </div>

            <div class="input-group">
                <label class="input-group__title">Date of publishing</label>
                <input type="text" name="created_at" title="Опубликован" class="input-regular"
                       value="{{ $group->created_at ?? '-' }}">
            </div>


            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                    <button type="submit" class="btn btn--green">Save</button>
                    @can('record-delete')
                        <a class="btn btn-danger btn--red" href="{{ route('panel.groups.destroy', $group->id) }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('delete-form').submit();" title="Удалить">Delete
                        </a>
                    @endcan
                </div>
            </div>
        </form>
        @can('group-delete')
            <form id="delete-form" action="{{ route('panel.groups.destroy', $group->id) }}" method="POST" class="d-none">
                @csrf
                @method('DELETE')
            </form>
        @endcan
    </div>
</div>

@endsection
@section('scripts')
    <!--Only this page's scripts-->
    <script src="https://cdn.tiny.cloud/1/eutvjr9zyhc4qtchwyfwhutknw2iunsf80kiuye2fdomu2wd/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script src="/admin/assets/api.js"></script>
    <script src="/admin/assets/js/tinymce.js"></script>
    <!---->
@endsection
